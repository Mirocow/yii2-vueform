<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class VueAsset
 * @package frontend\assets
 */
class VueAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
		public $sourcePath = '@mirocow/vueform/assets/app';
    public $css = [
    ];
    public $js = [
        /**
         * https://ru.vuejs.org/v2/guide/
         */
        YII_ENV_DEV? 'js/lib/vue.js' : 'js/lib/vue.min.js',

        /**
         * https://router.vuejs.org/ru/
         */
        YII_ENV_DEV? 'js/lib/vue-router.js': 'js/lib/vue-router.min.js',

        /**
         * https://vuex.vuejs.org/ru/intro.html
         */
        YII_ENV_DEV? 'js/lib/vuex.js': 'js/lib/vuex.min.js',

        /**
         * https://lodash.com/
         */
        YII_ENV_DEV? 'js/lib/lodash.js': 'js/lib/lodash.min.js',
    ];
    public $depends = [
    ];
}